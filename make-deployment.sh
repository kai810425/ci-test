cat >deployment.yaml <<EOL
apiVersion: apps/v1beta1
kind: Deployment
metadata:
  name: laravel-deployment
  labels:
    app: laravel
spec:
  selector:
    matchLabels:
      app: laravel
  minReadySeconds: 5
  strategy:
    # indicate which strategy we want for rolling update
    type: RollingUpdate
    rollingUpdate:
      maxSurge: $1
  template:
    metadata:
      labels:
        app: laravel
    spec:
      containers:
      - name: laravel
        image: $2
        ports:
        - containerPort: 80
EOL